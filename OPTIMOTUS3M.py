
# -*- coding: utf-8 -*-

#---------------------------------------------------------------------------------------
#----------------------- Le programme cherche les meilleurs TRIOLETS -------------------
#-------------------------CALCUL DU SCORE : LD+LI --------------------------------------
#---------------------------------------------------------------------------------------

import sys
import csv
  
def initialiser_dico(dico):
  lettres = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
  for i in range(7,11):
    for j in lettres:
      ID="%s%s"% (i,j)
      dico[ID] = {"MOTS" : [],"SCORE": 0 }
  return dico
               
def delete_double(l):
  lu = []
  for v in l:
    if v not in lu:
      lu.append(v)
  return lu
  
def ID_MOT(Mot): # Retourne le profil du mot (Longueur, Premiere lettre)
  #ID = "%s%s" %(len(Mot), Mot[0])
  ID = [len(Mot),Mot[0]]
  return ID

def LD(M1,M2,M3) : # Retourne le nombre de lettres differentes proposees
  LD = delete_double(M1+M2+M3)
  #print LD
  LD = len(LD)
  return LD
  
def LI(M1,M2,M3) : #Retourne le nombre de lettres importantes placees a differents endroits
  LI = []
  Lettres = ["EASINTRLUO","EASINTRLUODCPM","EASINTRLUODCPMVGFBQH","EASINTRLUODCPMVGFBQHXJYZKW"]
  for k in range(len(Lettres)):
    li=[]
    for i in range(len(M1)):
      if M1[i] in Lettres[k] :
          li.append(M1[i])
      if M2[i] in Lettres[k] :
        if M1[i] != M2[i]:
          li.append(M2[i])
      if M3[i] in Lettres[k] :
        if M3[i] != M1[i] and M3[i] != M2[i]:
          li.append(M2[i])
    LI.append(li)
    
  for k in range(len(LI)):
    LI[k] = len(LI[k])
  return LI

print "\nOPTIMOTUS te souhaite la bienvenue.\n"
f=open("dicoj.csv","r")
test=csv.reader(f,delimiter="\t")

f2=open("dicojbis.csv","r")
test2=csv.reader(f2,delimiter="\t")

f3=open("dicoj3.csv","r")
test3=csv.reader(f3,delimiter="\t")

BEST_SCORE = 0
BEST_MOTS= ""
i=0
b=0
Best_Mots = {}
Best_Mots = initialiser_dico(Best_Mots)
Best_Motous = {}
Best_Motous = initialiser_dico(Best_Motous)

"""IDs=[]
compteur=0
lettres = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
for a in range(7,11):
  for b in lettres:
    print IDs
    pst="%s%s" %(a,b)
    print pst
    IDs[compteur]=pst
    compteur+=1
print IDs"""

#----------------------------Recherche des meilleurs triolets ------------------------------------

for row1 in test:
  print row1
  b=i
  f2.seek(i)
  Mot1 = row1[0].upper()
  M1=list(Mot1)
  #print Mot1
  #print "\nMot1: ",Mot1
  ID_M1 = ID_MOT(M1)
  del M1[0]
  #z=0
  for row2 in test2:
    """z+=1
    if z<3:
      print "Mot2:",row2"""
    #print "b - ",b
    f3.seek(b)
    Mot2 = row2[0].upper()
    M2=list(Mot2)
    #print "Mot2: ",Mot2
    ID_M2 = ID_MOT(M2)
    if ID_M1 == ID_M2:
      del M2[0]
      #w=0
      for row3 in test3:
        """w+=1
        if w<3:
          print "Mot3:",row3"""
        Mot3 = row3[0].upper()
        M3=list(Mot3)
        #print "Mot3: ",Mot3
        ID_M3 = ID_MOT(M3)
        del M3[0]
    
        if ID_M1 == ID_M3:
          ID = "%s%s" %(ID_M1[0], ID_M1[1])
          LIs = LI(M1,M2,M3)
          #print LIs
          SCORE = LD(M1,M2,M3) + LIs[0]
          #print Mot1,Mot2,SCORE
          if SCORE == Best_Mots[ID]['SCORE']:
            mots=[Mot1,Mot2,Mot3]
            Best_Motous[ID]['MOTS'].append(mots)
          elif SCORE > Best_Mots[ID]['SCORE']:
            mots=[Mot1,Mot2,Mot3]
            Best_Motous = initialiser_dico(Best_Motous)
            Best_Motous[ID]['MOTS'].append(mots)
            Best_Mots[ID]['MOTS'] = Mot1,Mot2,Mot3
            Best_Mots[ID]['SCORE'] = SCORE
        else :
          pass
    else:
      pass
    b+=ID_M2[0]+1
    
  i+=ID_M1[0]+1

#------------------------------Afficher les meilleurs mots--------------------------------------------------

print Best_Mots
lettres = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
for i in range(7,11):
  for j in lettres:
    ID="%s%s"% (i,j)
    if Best_Mots[ID]['MOTS']:
      Best_Motous[ID]['MOTS']=delete_double(Best_Motous[ID]['MOTS'])
      
      print "\n",ID,":",
      print Best_Mots[ID]['SCORE'],"\n"
      print Best_Motous[ID]['MOTS'][0],
      
      k=0
      print len(Best_Motous[ID]['MOTS'])
      while(k<len(Best_Motous[ID]['MOTS'])):
        autresmots =raw_input("\tvoir un autre couple? 0/1\t")

        if autresmots == "2":
          k-=1
          if k==-1:
            k=len(Best_Motous[ID]['MOTS'])-1
          print Best_Motous[ID]['MOTS'][k],
          
        elif autresmots == "1":
          k+=1
          if(k==len(Best_Motous[ID]['MOTS'])):
            k=0
          print Best_Motous[ID]['MOTS'][k],
        
        elif autresmots == "3":
          k+=1
          if(k==len(Best_Motous[ID]['MOTS'])):
            k=0
          while(Best_Motous[ID]['MOTS'][k][0]==Best_Motous[ID]['MOTS'][k-1][0]):
            k+=1
            if(k==len(Best_Motous[ID]['MOTS'])):
              k=0
          print Best_Motous[ID]['MOTS'][k],
        
        elif autresmots == "4":
          k+=1
          if(k==len(Best_Motous[ID]['MOTS'])):
            k=0
          while(Best_Motous[ID]['MOTS'][k][1]==Best_Motous[ID]['MOTS'][k-1][1]):
            k+=1
            if(k==len(Best_Motous[ID]['MOTS'])):
              k=0
          print Best_Motous[ID]['MOTS'][k],
          
        elif autresmots == "0":
          Best_Mots[ID]['MOTS']=Best_Motous[ID]['MOTS'][k]
          break
        
        else:
          print "Commande inexistante."
        

#---------------------------------------Enregistrer les mots choisis------------------------------------------------

print "\n"
for i in range(7,11):
  for j in range(26):
    ID="%s%s"% (i,lettres[j])
    if Best_Mots[ID]['MOTS']:
      print ID,":",Best_Mots[ID]['MOTS']
print "\n"

q=input("Ajouter ces mots a la liste des optimots? 0/1\t")

if q==1:
  fin=open("mots_optimises3.csv","r")
  f_in=csv.reader(fin,delimiter="\t")

  fout=open("mots_optimises3.csv","a")
  f_out=csv.writer(fout,delimiter="\t")

  Mots_retenus = {}
  Mots_retenus = initialiser_dico(Mots_retenus)

  numlettre=0
  nombrelettre=7
  for row in f_in:
    if numlettre == 26:
      numlettre = 0
      nombrelettre += 1
    ID="%s%s"% (nombrelettre,lettres[numlettre])
    if Best_Mots[ID]['MOTS']:
      Mots_retenus[ID]['MOTS']=Best_Mots[ID]['MOTS']
    else:
      Mots_retenus[ID]['MOTS'] = row
    numlettre+=1
  
  fout.seek(0) 
  fout.truncate()
  for i in range(7,11):
    for j in range(26):
      ID="%s%s"% (i,lettres[j])
      yep = Mots_retenus[ID]['MOTS']
      f_out.writerow(yep)

else:
  pass
    
"""    
fout=open("mots_optimises.csv","a")
f_out=csv.writer(fout,delimiter="\t")

print "\n"
for i in range(7,11):
  for j in range(26):
    ID="%s%s"% (i,lettres[j])
    if Best_Mots[ID]['MOTS']:
      print ID,":",Best_Mots[ID]['MOTS']
      f_out.writerow(Best_Mots[ID]['MOTS'])
print "\n"

"""

